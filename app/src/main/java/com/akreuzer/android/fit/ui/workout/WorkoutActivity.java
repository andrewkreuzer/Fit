package com.akreuzer.android.fit.ui.workout;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.WorkoutActivityBinding;
import com.akreuzer.android.fit.repository.ExerciseInfoRepository;
import com.akreuzer.android.fit.ui.workout.currentworkout.CurrentWorkoutFragment.CurrentWorkoutFragmentCallbacks;
import com.akreuzer.android.fit.vo.ExerciseInfo;
import com.akreuzer.android.fit.vo.ExerciseRecord;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by andrewkreuzer on 2018-01-10.
 */

public class WorkoutActivity extends AppCompatActivity implements
        CurrentWorkoutFragmentCallbacks,
        HasSupportFragmentInjector {
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    private final String TAG = WorkoutActivity.class.getSimpleName();

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private WorkoutViewModel workoutViewModel;

    @Inject ExerciseInfoRepository exerciseInfoRepository;

    WorkoutActivityBinding binding;
    View container;

    @Inject
    WorkoutActivityNavigation navigation;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.workout_activity);
        container = binding.workoutFragmentContainer;

        workoutViewModel = ViewModelProviders.of(this, viewModelFactory).get(WorkoutViewModel.class);

        Toolbar workoutToolbar = binding.workoutToolbar;
        setSupportActionBar(workoutToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        navigation.toCurrentWorkout();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void openNewEditExerciseFragment(View v, ExerciseInfo exerciseInfo) {
        ExerciseRecord newExerciseRecord = workoutViewModel.createExerciseRecord(
                exerciseInfo.getId(),
                exerciseInfo.getName());
        workoutViewModel.addExerciseCurrentWorkout(newExerciseRecord);
        Bundle bundle = new Bundle();
        bundle.putString("exerciseId", newExerciseRecord.getId());
        navigation.toEditExercise(bundle);
    }

    @Override
    public void openEditExerciseFragment(View v, ExerciseRecord exerciseRecord) {
        Bundle bundle = new Bundle();
        bundle.putString("exerciseId", exerciseRecord.getId());
        navigation.toEditExercise(bundle);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}
