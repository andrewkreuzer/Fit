package com.akreuzer.android.fit.db.workouts;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.akreuzer.android.fit.vo.Workout;

import java.util.List;

/**
 * Created by andrewkreuzer on 2018-01-12.
 */

@Dao
public interface WorkoutDao {
    @Query("SELECT * FROM Workout")
    LiveData<List<Workout>> getAll();

    @Insert
    void insert(Workout... workoutEntities);

    @Delete
    void delete(Workout workout);
}
