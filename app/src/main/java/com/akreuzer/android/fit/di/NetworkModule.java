package com.akreuzer.android.fit.di;

import com.akreuzer.android.fit.api.ExerciseInfoService;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * akreuzer on 23/02/18.
 */

@Module
class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient() {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    @Provides
    @Singleton
    ExerciseInfoService provideExerciseInfoService(OkHttpClient client) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl("http://192.168.1.29:3000/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(ExerciseInfoService.class);
    }
}
