package com.akreuzer.android.fit.ui.main.pagerfragments;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.MainFragmentHomeBinding;
import com.akreuzer.android.fit.repository.WorkoutRepository;
import com.akreuzer.android.fit.ui.workout.WorkoutActivity;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

/**
 * akreuzer on 13/02/18.
 */

public class HomeFragment extends Fragment {

    MainFragmentHomeBinding binding;

    @Inject
    WorkoutRepository workoutRepository;
    @Inject
    Application context;

    @Inject
    public HomeFragment() {}

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Timber.i("HomeFragment");

        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment_home, container, false);
        View view = binding.getRoot();

        final Button button = binding.button;
        button.setOnClickListener(v -> {
            Toast.makeText(getContext(), "start a workout",
                    Toast.LENGTH_LONG).show();
            Intent workoutIntent = new Intent(getActivity().getApplicationContext(), WorkoutActivity.class);
            startActivity(workoutIntent);
        });

        return view;
    }
}
