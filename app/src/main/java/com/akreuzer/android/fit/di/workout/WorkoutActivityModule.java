package com.akreuzer.android.fit.di.workout;

import android.app.Activity;

import com.akreuzer.android.fit.ui.workout.WorkoutActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * akreuzer on 05/03/18.
 */

@Module(subcomponents = WorkoutActivitySubcomponent.class)
public abstract class WorkoutActivityModule {
    @Binds
    @IntoMap
    @ActivityKey(WorkoutActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
        bindWorkoutActivityInjectorFactory(WorkoutActivitySubcomponent.Builder builder);
}
