package com.akreuzer.android.fit.di.workout;

import com.akreuzer.android.fit.ui.workout.WorkoutActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * akreuzer on 05/03/18.
 */

@WorkoutActivityScope
@Subcomponent(modules = WorkoutFragmentsModule.class)
public interface WorkoutActivitySubcomponent extends AndroidInjector<WorkoutActivity> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<WorkoutActivity> {}
}
