package com.akreuzer.android.fit.ui.workout.currentworkout;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.akreuzer.android.fit.repository.ExerciseInfoRepository;
import com.akreuzer.android.fit.vo.ExerciseInfo;
import com.akreuzer.android.fit.vo.ExerciseRecord;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

/**
 * akreuzer on 14/03/18.
 */

public class CurrentWorkoutViewModel extends ViewModel {
    private ExerciseInfoRepository exerciseInfoRepository;

    private MediatorLiveData<List<ExerciseRecord>> exerciseRecords = new MediatorLiveData<>();
    private MediatorLiveData<List<ExerciseInfo>> exerciseQuery = new MediatorLiveData<>();

    @Inject
    public CurrentWorkoutViewModel(ExerciseInfoRepository exerciseInfoRepository) {
        this.exerciseInfoRepository = exerciseInfoRepository;
        this.exerciseRecords.setValue(Collections.emptyList());
        this.exerciseQuery.setValue(Collections.emptyList());
    }

    public void setupExerciseInfoDatabase() {
        exerciseInfoRepository.fillExerciseInfoDatabase();
    }

    public void setExerciseRecords(List<ExerciseRecord> exerciseRecords) {
        this.exerciseRecords.setValue(exerciseRecords);
    }

    public LiveData<List<ExerciseRecord>> getExerciseRecords() {
        return exerciseRecords;
    }

    public MutableLiveData<List<ExerciseInfo>> getExerciseQuery() {
        return exerciseQuery;
    }

    public void setExerciseQuery(List<ExerciseInfo> exerciseQuery) {
        this.exerciseQuery.setValue(exerciseQuery);
    }

    public void searchExercises(String query) {
        exerciseQuery.addSource(exerciseInfoRepository.exerciseSearch(query), exerciseQuery::setValue);
    }

}
