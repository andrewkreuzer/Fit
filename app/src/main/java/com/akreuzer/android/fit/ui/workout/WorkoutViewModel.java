package com.akreuzer.android.fit.ui.workout;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.ArrayMap;
import android.util.Log;

import com.akreuzer.android.fit.vo.ExerciseRecord;
import com.akreuzer.android.fit.vo.Set;
import com.akreuzer.android.fit.vo.Workout;
import com.akreuzer.android.fit.repository.WorkoutRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by andrewkreuzer on 2018-01-19.
 */

public class WorkoutViewModel extends AndroidViewModel {

    private final String TAG = WorkoutViewModel.class.getSimpleName();

    private final WorkoutRepository workoutRepository;
    private LiveData<List<Workout>> allWorkouts;

    private MutableLiveData<Map<String, ExerciseRecord>> exerciseList;

    @Inject
    public WorkoutViewModel(Application application, WorkoutRepository workoutRepository) {
        super(application);

        this.workoutRepository = workoutRepository;
//        allWorkouts = repository.getAllWorkouts();

        if (exerciseList == null) {
            exerciseList = new MutableLiveData<>();
            exerciseList.setValue(new ArrayMap<>());
        }
    }

    public void saveWorkout() {
        Workout workout = new Workout(new Date());
        Map<String, ExerciseRecord> exerciseRecordList = exerciseList.getValue();
        if (exerciseRecordList != null) {
            workout.setExercisesList(new ArrayList<>(exerciseRecordList.values()));
        }
        workoutRepository.insert(workout);
    }

    public int getExerciseCount() {
        if (exerciseList.getValue() != null) {
            return exerciseList.getValue().isEmpty() ? 0 : exerciseList.getValue().size();
        }
        return 0;
    }

    public LiveData<Map<String, ExerciseRecord>> getExercises() { return exerciseList; }

    public ExerciseRecord getExercise(String exerciseId) {
        return exerciseList.getValue().get(exerciseId);
    }

    public void setExerciseList(MutableLiveData<Map<String, ExerciseRecord>> exerciseList) {
        this.exerciseList = exerciseList;
    }

    public Map<String, ExerciseRecord> getExerciseListValue() {
        return this.exerciseList.getValue();
    }

    public void addExerciseCurrentWorkout(ExerciseRecord exerciseRecord) {
        Map<String, ExerciseRecord> exerciseRecordMap = this.exerciseList.getValue();
        if (exerciseRecordMap != null) {
            exerciseRecordMap.put(exerciseRecord.getId(), exerciseRecord);
            this.exerciseList.setValue(exerciseRecordMap);
        } else {
            Timber.d(TAG, "addExerciseCurrentWorkout: exerciseRecordList null");
        }
    }

    public ExerciseRecord createExerciseRecord(String infoId, String name) {
        return new ExerciseRecord(
                UUID.randomUUID().toString(),
                infoId,
                name
        );
    }

    public void updateExerciseRecord(String recordId, ExerciseRecord exerciseRecord) {
        Map<String, ExerciseRecord> exerciseRecordMap = this.exerciseList.getValue();
        if (exerciseRecordMap != null) {
            exerciseRecordMap.replace(recordId, exerciseRecord);
            this.exerciseList.postValue(exerciseRecordMap);
        }

    }

    public LiveData<List<Workout>> getAllWorkoutsList() { return allWorkouts; }
//    public void insert(Workout workout) { repository.insert(workout); }
}
