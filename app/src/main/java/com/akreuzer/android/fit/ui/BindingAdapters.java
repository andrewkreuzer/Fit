package com.akreuzer.android.fit.ui;

import android.databinding.BindingAdapter;
import android.view.View;

/**
 * Created by akreuzer on 28/01/18.
 */

public class BindingAdapters {
    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean visibility) {
        view.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }
}
