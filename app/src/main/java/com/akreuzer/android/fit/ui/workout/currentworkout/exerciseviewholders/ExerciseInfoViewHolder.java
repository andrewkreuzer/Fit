package com.akreuzer.android.fit.ui.workout.currentworkout.exerciseviewholders;

import android.view.View;

import com.akreuzer.android.fit.databinding.WorkoutCurrentExerciseInfoListItemBinding;
import com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseAdapter;
import com.akreuzer.android.fit.vo.ExerciseInfo;

/**
 * akreuzer on 16/03/18.
 */

public class ExerciseInfoViewHolder extends BaseExerciseViewHolder<ExerciseInfo> implements
        View.OnClickListener {
    final WorkoutCurrentExerciseInfoListItemBinding binding;
    private ExerciseInfo exerciseInfo;
    private ExerciseAdapter.InfoCallback callback;

    public ExerciseInfoViewHolder(WorkoutCurrentExerciseInfoListItemBinding binding,
                                  ExerciseAdapter.InfoCallback callback) {
        super(binding.getRoot());

        this.binding = binding;
        this.callback = callback;

        binding.getRoot().setOnClickListener(this);
    }

    @Override
    public void bind(ExerciseInfo exerciseInfo) {
        binding.setExercise(exerciseInfo);
        this.exerciseInfo = exerciseInfo;
    }

    @Override
    public void onClick(View v) {
        callback.onItemClicked(v, exerciseInfo);
    }
}
