package com.akreuzer.android.fit.api;

import com.akreuzer.android.fit.vo.ExerciseInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * akreuzer on 21/02/18.
 */

public interface ExerciseInfoService {
    @GET("exercise-list")
    Call<ExerciseInfo[]> getFullExerciseList();

    @POST("exercises.json")
    Call<String> postExercise(@Body ExerciseInfo exerciseInfo);

//    @GET("users/{login}/repos")
//    LiveData<ApiResponse<List<Repo>>> getRepos(@Path("login") String login);
//
//    @GET("repos/{owner}/{name}")
//    LiveData<ApiResponse<Repo>> getRepo(@Path("owner") String owner, @Path("name") String name);
//
//    @GET("repos/{owner}/{name}/contributors")
//    LiveData<ApiResponse<List<Contributor>>> getContributors(@Path("owner") String owner, @Path("name") String name);
//
//    @GET("search/repositories")
//    LiveData<ApiResponse<RepoSearchResponse>> searchRepos(@Query("q") String query);
//
//    @GET("search/repositories")
//    Call<RepoSearchResponse> searchRepos(@Query("q") String query, @Query("page") int page);
}
