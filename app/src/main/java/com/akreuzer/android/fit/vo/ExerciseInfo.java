package com.akreuzer.android.fit.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.akreuzer.android.fit.db.StringListJsonConverter;
import com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseViewTypes;
import com.squareup.moshi.Json;

import java.util.List;

@Entity
@TypeConverters(StringListJsonConverter.class)
public class ExerciseInfo implements ExerciseBaseModel {
    @PrimaryKey
    @NonNull
    @Json(name = "_id")
    public String id;
    private String name;
    private List<String> steps;
    @Json(name = "exercise_type")
    private List<String> exerciseType;
    @Json(name = "primary_muscles")
    private List<String> primaryMuscle;
    @Json(name = "secondary_muscles")
    private List<String> secondaryMuscles;
    private List<String> equipment;
    @Json(name = "image_urls")
    private List<String> imageURLs;
    @Json(name = "file_paths")
    private List<String> filePaths;

    public ExerciseInfo (String name,
                         List<String> steps,
                         List<String> exerciseType,
                         List<String> primaryMuscle, List<String> secondaryMuscles,
                         List<String> equipment,
                         List<String> imageURLs,
                         List<String> filePaths) {
        this.name = name;
        this.steps = steps;
        this.exerciseType = exerciseType;
        this.primaryMuscle = primaryMuscle;
        this.secondaryMuscles = secondaryMuscles;
        this.equipment = equipment;
        this.imageURLs = imageURLs;
        this.filePaths = filePaths;
    }

    @NonNull
    public String getId() { return this.id; }

    public List<String> getSteps() {
        return this.steps;
    }

    public List<String> getExerciseType() {
        return this.exerciseType;
    }

    public List<String> getPrimaryMuscle() {
        return this.primaryMuscle;
    }

    public List<String> getSecondaryMuscles() {
        return this.secondaryMuscles;
    }

    public List<String> getEquipment() {
        return this.equipment;
    }

    public List<String> getImageURLs() {
        return this.imageURLs;
    }

    public List<String> getFilePaths() {
        return this.filePaths;
    }

    @NonNull
    @Override
    public String getInfoId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getViewType() {
        return ExerciseViewTypes.ViewType.EXERCISE_INFO_TYPE;
    }
}
