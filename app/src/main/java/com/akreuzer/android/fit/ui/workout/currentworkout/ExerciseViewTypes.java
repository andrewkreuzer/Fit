package com.akreuzer.android.fit.ui.workout.currentworkout;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseViewTypes.ViewType.EXERCISE_INFO_TYPE;
import static com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseViewTypes.ViewType.EXERCISE_RECORD_TYPE;

/**
 * akreuzer on 15/03/18.
 */

public class ExerciseViewTypes {

    @IntDef({EXERCISE_RECORD_TYPE, EXERCISE_INFO_TYPE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewType {
        int EXERCISE_RECORD_TYPE = 100;
        int EXERCISE_INFO_TYPE = 200;
    }
}
