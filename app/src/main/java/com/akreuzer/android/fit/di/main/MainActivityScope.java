package com.akreuzer.android.fit.di.main;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * akreuzer on 26/02/18.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainActivityScope {
}
