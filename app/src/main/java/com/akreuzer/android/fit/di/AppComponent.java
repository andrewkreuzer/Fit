package com.akreuzer.android.fit.di;

import android.app.Application;

import com.akreuzer.android.fit.FitApp;
import com.akreuzer.android.fit.di.db.DatabaseModule;
import com.akreuzer.android.fit.di.main.MainActivityModule;
import com.akreuzer.android.fit.di.main.MainFragmentsModule;
import com.akreuzer.android.fit.di.workout.WorkoutActivityModule;


import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * akreuzer on 23/02/18.
 */

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        DatabaseModule.class,
        NetworkModule.class,
        MainActivityModule.class,
        WorkoutActivityModule.class,
        ViewModelModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        AppComponent build();
    }

    void inject(FitApp fitApp);
}
