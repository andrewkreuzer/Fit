package com.akreuzer.android.fit.db.exercises;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.akreuzer.android.fit.vo.ExerciseInfo;

import java.util.List;

/**
 * akreuzer on 21/02/18.
 */

@Dao
public interface ExerciseInfoDao {
    @Query("SELECT * FROM ExerciseInfo")
    LiveData<List<ExerciseInfo>> getAll();

    @Query("SELECT * FROM ExerciseInfo WHERE name = :name LIMIT 1")
    LiveData<ExerciseInfo> getExercise(String name);

    @Query("SELECT * FROM `ExerciseInfo` WHERE `name` LIKE 'b%' LIMIT 15")
    List<ExerciseInfo> test();

    @Query("SELECT * FROM `ExerciseInfo` WHERE `name` LIKE :query LIMIT 15")
    LiveData<List<ExerciseInfo>> search(String query);


    @Insert
    void insert(ExerciseInfo... exerciseInfoEntities);

    @Delete
    void delete(ExerciseInfo exerciseInfo);
}
