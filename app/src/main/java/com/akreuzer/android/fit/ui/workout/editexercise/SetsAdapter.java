package com.akreuzer.android.fit.ui.workout.editexercise;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.WorkoutEditExerciseSetListItemBinding;
import com.akreuzer.android.fit.vo.Set;

import java.util.List;

/**
 * akreuzer on 06/02/18.
 */

public class SetsAdapter extends
        RecyclerView.Adapter<SetsViewHolder> {

    private final String TAG = SetsViewHolder.class.getSimpleName();

    private List<Set> setList;

    TextChangeCallback callback;

    public SetsAdapter(TextChangeCallback callback) {
    }

    @Override
    public SetsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        WorkoutEditExerciseSetListItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.workout_edit_exercise_set_list_item,
                        parent, false);

        return new SetsViewHolder(binding, callback);
    }

    @Override
    public void onBindViewHolder(SetsViewHolder holder, int position) {
        holder.bind(setList.get(position));
        // holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() { return setList == null ? 0 : setList.size(); }

//    public List<Set> getSetList() { return setList; }

    public void update(final List<Set> newSetList) {
        int oldSetListSize = getItemCount();
        this.setList = newSetList;
        notifyDataSetChanged();
    }

    interface TextChangeCallback {
        void onWeightChange(int weight);
        void onRepsChange(int reps);
    }
}
