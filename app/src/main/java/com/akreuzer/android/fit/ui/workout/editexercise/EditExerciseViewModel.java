package com.akreuzer.android.fit.ui.workout.editexercise;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;


import com.akreuzer.android.fit.vo.ExerciseRecord;
import com.akreuzer.android.fit.vo.Set;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * akreuzer on 07/02/18.
 */

public class EditExerciseViewModel extends ViewModel {

    private String exerciseId;
    private String exerciseInfoId;
    private MutableLiveData<String> exerciseName;
    private MutableLiveData<List<Set>> setList;
    private MutableLiveData<String> exerciseNotes;

    @Inject
    public EditExerciseViewModel() {
        this.exerciseName = new MutableLiveData<>();
        this.exerciseNotes = new MutableLiveData<>();
        if (this.setList == null) {
            this.setList = new MutableLiveData<>();
            this.setList.setValue(new ArrayList<>());
        } else {
            Timber.i("SetList in newExerciseViewModel is null");
        }
    }

    public void setupFromExerciseRecord(ExerciseRecord exerciseRecord) {
        this.exerciseName.setValue(exerciseRecord.getName());
        this.exerciseId = exerciseRecord.getId();
        this.exerciseInfoId = exerciseRecord.getInfoId();
        if (exerciseRecord.getSets() != null) {
            this.setList.setValue(exerciseRecord.getSets());
        }
    }

    public ExerciseRecord exerciseRecordFromValues() {
        return  new ExerciseRecord(
                this.exerciseId,
                this.exerciseInfoId,
                this.exerciseName.getValue(),
                this.setList.getValue());
    }

    public LiveData<String> getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName.setValue(exerciseName);
    }

    public LiveData<List<Set>> getSetList() { return this.setList; }

    public void setSetList(List<Set> newSetList) { this.setList.setValue(newSetList); }

    public void addSet(int weight, int reps, @Nullable String notes) {
        List<Set> newSetList = this.setList.getValue();
        newSetList.add(new Set(
                newSetList.isEmpty() ? 1 : newSetList.size() + 1,
                weight,
                reps,
                notes
        ));
        this.setList.setValue(newSetList);
    }

    public void addNewSet() {
        List<Set> newSetList = this.setList.getValue();
        newSetList.add(new Set(
                newSetList.isEmpty() ? 1 : newSetList.size() + 1,
                0,
                0,
                null
        ));
        this.setList.setValue(newSetList);

    }

    public String getExerciseNotes() {
        return exerciseNotes.getValue();
    }
    public void setExerciseNotes(String exerciseNotes) {
        this.exerciseNotes.setValue(exerciseNotes);
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }
}
