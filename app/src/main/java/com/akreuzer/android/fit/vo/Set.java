package com.akreuzer.android.fit.vo;

import android.support.annotation.Nullable;

/**
 * akreuzer on 06/02/18.
 */

public class Set {

    private int id;

    private int weight;
    private int reps;

    private String notes;

    public Set(int id, int weight, int reps, String notes) {
        this.id = id;
        this.weight = weight;
        this.reps = reps;
        this.notes = notes;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
