package com.akreuzer.android.fit.ui.workout.currentworkout;

import android.support.v7.util.DiffUtil;

import com.akreuzer.android.fit.vo.ExerciseBaseModel;
import com.akreuzer.android.fit.vo.ExerciseInfo;
import com.akreuzer.android.fit.vo.ExerciseRecord;

import java.util.List;
import java.util.Objects;

/**
 * akreuzer on 14/03/18.
 */

public class ExerciseDiff<T extends ExerciseBaseModel> extends DiffUtil.Callback {
    private List<T> oldList;
    private List<T> newList;

    public ExerciseDiff(List<T> oldList, List<T> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(oldList.get(oldItemPosition).getInfoId(),
                newList.get(newItemPosition).getInfoId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        T oldItem = oldList.get(oldItemPosition);
        T newItem = newList.get(newItemPosition);
        return oldItem.getInfoId().equals(newItem.getInfoId()) &&
                oldItem.getName().equals(newItem.getName());
    }
}
