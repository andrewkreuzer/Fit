package com.akreuzer.android.fit.db.workouts;

import android.arch.persistence.room.TypeConverter;

import com.akreuzer.android.fit.vo.ExerciseRecord;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by akreuzer on 19/01/18.
 */

public class ExerciseRecordConverter {

    @TypeConverter
    public static List<ExerciseRecord> jsonToExerciseList(String json) {
        if (json == null) return Collections.emptyList();

        List<ExerciseRecord> exerciseRecordList = null;
        try {
            exerciseRecordList = moshiAdapter().fromJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exerciseRecordList;
    }

    @TypeConverter
    public static String exerciseListToJson(List<ExerciseRecord> exerciseRecordList) {

        String json = moshiAdapter().toJson(exerciseRecordList);
        return json;
    }

    private static JsonAdapter<List<ExerciseRecord>> moshiAdapter() {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, ExerciseRecord.class);
        JsonAdapter<List<ExerciseRecord>> adapter = moshi.adapter(type);

        return adapter;
    }
}
