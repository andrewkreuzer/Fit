package com.akreuzer.android.fit.di.workout;

import com.akreuzer.android.fit.ui.workout.currentworkout.CurrentWorkoutFragment;
import com.akreuzer.android.fit.ui.workout.editexercise.EditExerciseFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * akreuzer on 05/03/18.
 */
@Module
public abstract class WorkoutFragmentsModule {
    @ContributesAndroidInjector
    abstract EditExerciseFragment contributeEditExerciseFragment();

    @ContributesAndroidInjector
    abstract CurrentWorkoutFragment contributeCurrentWorkoutFragment();
}
