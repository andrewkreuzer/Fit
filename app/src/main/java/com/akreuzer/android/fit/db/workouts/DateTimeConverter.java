package com.akreuzer.android.fit.db.workouts;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * akreuzer on 12/02/18.
 */

public class DateTimeConverter {

//    @RequiresApi(api = Build.VERSION_CODES.O)
//    @TypeConverter
//    public Instant InstantFromTimestamp(Long value) {
//        return value == null ? null : Instant.ofEpochMilli(value);
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    @TypeConverter
//    public Long InstantToTimeStamp(Instant instant) {
//        if (instant == null) return null;
//        else {
//            return instant.toEpochMilli();
//        }
//    }

    @TypeConverter
    public Date dateFromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) return null;
        else {
            return date.getTime();
        }
    }
}
