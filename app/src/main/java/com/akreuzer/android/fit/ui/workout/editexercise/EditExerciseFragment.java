package com.akreuzer.android.fit.ui.workout.editexercise;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.WorkoutEditExerciseFragmentBinding;
import com.akreuzer.android.fit.ui.workout.WorkoutActivity;
import com.akreuzer.android.fit.ui.workout.WorkoutViewModel;
import com.akreuzer.android.fit.vo.ExerciseRecord;

import java.util.concurrent.Executors;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;


/**
 * akreuzer on 30/01/18.
 */

public class EditExerciseFragment extends Fragment {
    //TODO: set these from settings?
    private static final int WEIGHT_DECREMENT = 5;
    private static final int WEIGHT_INCREMENT = 5;

    @Inject
    WorkoutActivity workoutActivity;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private EditExerciseViewModel editExerciseViewModel;
    private WorkoutViewModel workoutViewModel;

    private ActionBar workoutActionBar;

    private WorkoutEditExerciseFragmentBinding binding;
    private RecyclerView setListRecyclerView;
    private SetsAdapter setsAdapter;
    private LinearLayoutManager layoutManager;

    private EditText repsEditText;
    private EditText weightEditText;
    private EditText notesEditText;

    @Inject
    public EditExerciseFragment() {}

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
//        // ensures the callbacks have been implemented by the base activity
//        try {
//            callbacksListener = (EditExerciseFragmentCallbacks) context;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(context.toString()
//                    + " WorkoutActivityCallbacks need to be implemented");
//        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editExerciseViewModel = ViewModelProviders.of(this, viewModelFactory).get(EditExerciseViewModel.class);
        setupEditorObservers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Executors.newSingleThreadExecutor().execute(this::saveExercise);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil
                .inflate(inflater, R.layout.workout_edit_exercise_fragment, container, false);
        binding.setClickCallback(clickCallback);
        View view = binding.getRoot();

        setHasOptionsMenu(true);
        workoutActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();


        setListRecyclerView = binding.setListRecyclerView;
        setListRecyclerView.setHasFixedSize(true);
        setsAdapter = new SetsAdapter(new SetsAdapter.TextChangeCallback() {
            @Override
            public void onWeightChange(int weight) {
                //TODO
                // having the view holder pass back the set to add to the setlist
                // will cause the observer to fire, update the viewholder and cause a loop
                // the process of passing changes in the edittext fields back to update the setList
                // in the viewmodel will have to be rethought
                editExerciseViewModel.setWeight(weight);
            }

            @Override
            public void onRepsChange(int reps) {
                editExerciseViewModel.setReps(reps);
            }
        });
        setListRecyclerView.setAdapter(setsAdapter);
        layoutManager = new LinearLayoutManager(getActivity());
        setListRecyclerView.setLayoutManager(layoutManager);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        workoutViewModel = ViewModelProviders.of(workoutActivity, viewModelFactory).get(WorkoutViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            ExerciseRecord exerciseRecord = workoutViewModel.getExercise(bundle.getString("exerciseId"));
            editExerciseViewModel.setupFromExerciseRecord(exerciseRecord);
        }
    }

    private void setupEditorObservers() {
        //TODO: title workout not exercise
        editExerciseViewModel.getExerciseName().observe(this, name -> workoutActionBar.setTitle(name));
        editExerciseViewModel.getSetList().observe(this, sets -> setsAdapter.update(sets));
//        editExerciseViewModel.getReps().observe(this, reps ->
//                repsEditText.setText(String.valueOf(reps)));
//        editExerciseViewModel.getWeight().observe(this, weight ->
//                weightEditText.setText(String.valueOf(weight)));
    }

    //TODO: remove the EditorCallabck?
    ClickCallback clickCallback = v -> {
//        int weight = editExerciseViewModel.getWeightValue();
//        int reps = editExerciseViewModel.getRepsValue();

        switch (v.getId()) {
//            case R.id.edit_exercise_weight_minus:
//                if (weight < 1) break;
//                weightEditText.setText(String.valueOf(weight - WEIGHT_DECREMENT));
//                break;
//
//            case R.id.edit_exercise_weight_plus:
//                weightEditText.setText(String.valueOf(weight + WEIGHT_INCREMENT));
//                break;
//
//            case R.id.edit_exercise_reps_minus:
//                if (reps < 1) break;
//                repsEditText.setText(String.valueOf(reps - 1));
//                break;
//
//            case R.id.edit_exercise_reps_plus:
//               repsEditText.setText(String.valueOf(reps + 1));
//                break;

            case R.id.add_set:
                editExerciseViewModel.addNewSet();
                break;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.editexercise, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                saveExercise();
                workoutActivity.onBackPressed();
                break;
            //TODO: make a settings menuItem?
        }
        return true;
    }

    public void saveExercise() {
        workoutViewModel.updateExerciseRecord(
                editExerciseViewModel.getExerciseId(),
                editExerciseViewModel.exerciseRecordFromValues());
    }
}
