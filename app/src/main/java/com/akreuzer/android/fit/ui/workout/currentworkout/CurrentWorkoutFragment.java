package com.akreuzer.android.fit.ui.workout.currentworkout;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.WorkoutCurrentFragmentBinding;
import com.akreuzer.android.fit.ui.workout.WorkoutViewModel;
import com.akreuzer.android.fit.utils.KeyboardUtils;
import com.akreuzer.android.fit.vo.ExerciseBaseModel;
import com.akreuzer.android.fit.vo.ExerciseInfo;
import com.akreuzer.android.fit.vo.ExerciseRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * Created by akreuzer on 19/01/18.
 */

public class CurrentWorkoutFragment extends Fragment {

    private final String TAG = CurrentWorkoutFragment.class.getSimpleName();

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private WorkoutViewModel workoutViewModel;
    private CurrentWorkoutViewModel currentWorkoutViewModel;

    @Inject
    KeyboardUtils keyboardUtils;

    @Inject SharedPreferences sharedPreferences;

    ActionBar workoutActionBar;

    private WorkoutCurrentFragmentBinding binding;
    private RecyclerView exerciseRecyclerView;
    private ExerciseAdapter exerciseAdapter;
    private LinearLayoutManager layoutManager;

    private FloatingActionButton addExerciseFab;
    private CurrentWorkoutFragmentCallbacks callbacksListener;

    public interface CurrentWorkoutFragmentCallbacks {
        void openNewEditExerciseFragment(View v, ExerciseInfo exerciseInfo);
        void openEditExerciseFragment(View v, ExerciseRecord exerciseRecord);
    }

    @Inject
    public CurrentWorkoutFragment() {}

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        // ensures the callbacks have been implemented by the base activity
        try {
            callbacksListener = (CurrentWorkoutFragmentCallbacks) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " CurrentWorkoutFragmentCallbacks need to be implemented");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.currentWorkoutViewModel = ViewModelProviders.of(this, viewModelFactory).get(CurrentWorkoutViewModel.class);
        currentWorkoutViewModel.getExerciseQuery().observe(this, exerciseInfo -> {
                if (!exerciseInfo.isEmpty()) {
                    exerciseAdapter.setExerciseList(exerciseInfo);
                    exerciseRecyclerView.scrollToPosition(0);
                }
        });
        //TODO: not sure if i actually want to observe this, don't want to add exercises then have
        //the recycler change while user still may be searching
        currentWorkoutViewModel.getExerciseRecords().observe(this, exerciseRecords -> {
            if (!exerciseRecords.isEmpty()) {
                exerciseAdapter.setExerciseList(exerciseRecords);
                exerciseRecyclerView.scrollToPosition(0);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Executors.newSingleThreadExecutor().execute(this::completeWorkout);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         binding = DataBindingUtil
                .inflate(inflater, R.layout.workout_current_fragment, container, false);
        View view = binding.getRoot();

        setHasOptionsMenu(true);
        workoutActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        setupRecyclerView();
        setupExerciseInfoDatabase();
        setupFab();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        workoutViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(WorkoutViewModel.class);
        workoutViewModel.getExercises().observe(this, exercises -> {
                if (!exercises.isEmpty()) {
                    exerciseAdapter.setExerciseList(new ArrayList<>(exercises.values()));
                }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
        }
        return true;
    }

    private void setupRecyclerView() {
        exerciseRecyclerView = binding.exerciseRecyclerView;
        exerciseRecyclerView.setHasFixedSize(true);
        exerciseAdapter = new ExerciseAdapter(
                (v, exerciseInfo) -> {
                    closeSearchToolbar();
                    callbacksListener.openNewEditExerciseFragment(v, exerciseInfo);
                },
                (v, exerciseRecord) -> {
                    closeSearchToolbar();
                    callbacksListener.openEditExerciseFragment(v, exerciseRecord);
                });
        exerciseRecyclerView.setAdapter(exerciseAdapter);
        layoutManager = new LinearLayoutManager(getActivity());
        exerciseRecyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(
                exerciseRecyclerView.getContext(),
                layoutManager.getOrientation());
        itemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.list_divider));
        exerciseRecyclerView.addItemDecoration(itemDecoration);
    }

    private void setupExerciseInfoDatabase() {
        boolean exerciseInfoFilled = sharedPreferences.getBoolean("ExerciseInfoDatabaseFilled", false);
        if (!exerciseInfoFilled) {
            Snackbar.make(binding.currentWorkoutCoordinatorLayout,
                "fill the exercise database?",
                Snackbar.LENGTH_INDEFINITE)
            .setAction("Yes", v ->
                currentWorkoutViewModel.setupExerciseInfoDatabase())
            .show();
        }
    }

    private void setupFab() {
        addExerciseFab = binding.addExerciseFab;
        addExerciseFab.setOnClickListener(v -> openSearchToolbar());
    }

    private void openSearchToolbar() {
        SearchView workoutSearchView = new SearchView(getActivity());
        workoutSearchView.setFocusable(true);
        workoutSearchView.setIconified(false);
        workoutSearchView.requestFocusFromTouch();
        workoutSearchView.setOnQueryTextListener(searchToolbarTextListener);
        workoutSearchView.setOnCloseListener(searchToolbarCloseListener);
        workoutActionBar.setCustomView(workoutSearchView);
        workoutActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //TODO: animate the FAB
    }

    SearchView.OnQueryTextListener searchToolbarTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        //TODO: create thread to handle text changes and add delay to remove recyclerview jank
        @Override
        public boolean onQueryTextChange(String newText) {
            if (newText.isEmpty()) return false;
            currentWorkoutViewModel.searchExercises(newText);
            return false;
        }
    };

    SearchView.OnCloseListener searchToolbarCloseListener = new SearchView.OnCloseListener() {
        @Override
        public boolean onClose() {
            Collection<? extends ExerciseBaseModel> exercises = workoutViewModel.getExercises().getValue().values();
            exerciseAdapter.setExerciseList(new ArrayList<>(exercises));
            keyboardUtils.hideSoftKeyboard(getActivity().getCurrentFocus().getRootView(), getActivity());
            closeSearchToolbar();
            return false;
        }
    };

    private void closeSearchToolbar() {
        workoutActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE
                | ActionBar.DISPLAY_HOME_AS_UP);
    }

    private void completeWorkout() {
        workoutViewModel.saveWorkout();
    }
}