package com.akreuzer.android.fit.vo;

import android.support.annotation.Nullable;

import com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseViewTypes;

import java.util.List;

/**
 * Created by akreuzer on 19/01/18.
 */

public class ExerciseRecord implements ExerciseBaseModel {

    private String id;
    private String name;
    private String infoId;

    private List<Set> sets;

    public ExerciseRecord(String id, String infoId, String name) {
        this.id = id;
        this.infoId = infoId;
        this.name = name;
    }

    public ExerciseRecord(String id, String infoId, String name, List<Set> sets) {
        this.id = id;
        this.name = name;
        this.infoId = infoId;
        this.sets = sets;
    }

    public String getId() { return id; }

    public void setInfoId(String infoId) {
        this.infoId = infoId;
    }

    public List<Set> getSets() {
        return sets;
    }

    public void setSets(List<Set> sets) {
        this.sets = sets;
    }

    @Override
    public String getInfoId() {
        return this.infoId;
    }
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getViewType() {
        return ExerciseViewTypes.ViewType.EXERCISE_RECORD_TYPE;
    }
}
