package com.akreuzer.android.fit.ui.workout.currentworkout.exerciseviewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * akreuzer on 15/03/18.
 */


public abstract class BaseExerciseViewHolder<T> extends RecyclerView.ViewHolder {

    public BaseExerciseViewHolder(View itemView) {
        super(itemView);
    }
    public abstract void bind(T object);
}


