package com.akreuzer.android.fit.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.akreuzer.android.fit.db.workouts.WorkoutDao;
import com.akreuzer.android.fit.db.workouts.WorkoutDatabase;
import com.akreuzer.android.fit.vo.Workout;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

/**
 * Created by andrewkreuzer on 2018-01-17.
 */

@Singleton
public class WorkoutRepository {
    private WorkoutDatabase db;
    private WorkoutDao workoutDao;
    private LiveData<List<Workout>> allWorkouts;

    @Inject
    public WorkoutRepository(WorkoutDatabase db, WorkoutDao workoutDao) {
        this.db = db;
        this.workoutDao = workoutDao;
        this.allWorkouts =  workoutDao.getAll();
    }

    public LiveData<List<Workout>> getAllWorkouts() {
        Timber.i("gettingWorkouts");
        return allWorkouts;
    }


    public void insert(Workout workout) {
        Executors.newSingleThreadScheduledExecutor().execute(() -> workoutDao.insert(workout));
    }
}
