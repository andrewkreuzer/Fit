package com.akreuzer.android.fit.di.workout;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * akreuzer on 05/03/18.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface WorkoutActivityScope {
}
