package com.akreuzer.android.fit.di.main;

import android.app.Activity;

import com.akreuzer.android.fit.ui.main.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;

import dagger.multibindings.IntoMap;

/**
 * akreuzer on 23/02/18.
 */

@Module(subcomponents = MainActivitySubcomponent.class)
public abstract class MainActivityModule {
    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
        bindMainActivityInjectorFactory(MainActivitySubcomponent.Builder builder);
}
