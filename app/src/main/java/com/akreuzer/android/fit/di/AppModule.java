package com.akreuzer.android.fit.di;


import android.app.Application;
import android.content.SharedPreferences;

import com.akreuzer.android.fit.utils.KeyboardUtils;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * akreuzer on 23/02/18.
 */

@Module
class AppModule {
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Application application) {
        return application.getSharedPreferences("Fit", Application.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    KeyboardUtils provideKeyboardUtils() {
        return new KeyboardUtils();
    }
}
