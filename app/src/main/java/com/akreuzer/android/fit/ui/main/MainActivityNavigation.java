package com.akreuzer.android.fit.ui.main;



import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.ui.main.pagerfragments.HomeFragment;
import com.akreuzer.android.fit.ui.workout.WorkoutActivity;
import com.akreuzer.android.fit.ui.workout.currentworkout.CurrentWorkoutFragment;
import com.akreuzer.android.fit.ui.workout.editexercise.EditExerciseFragment;

import javax.inject.Inject;

/**
 * akreuzer on 08/02/18.
 */

//TODO: this has to be dealt with, its not a good way to do this
public class MainActivityNavigation {
    private final Activity activity;
    private final int mainContainerId;
    private final FragmentManager fragmentManager;

    @Inject
    public MainActivityNavigation(MainActivity mainActivity) {
        this.activity = mainActivity;
        this.mainContainerId = 0;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void toHome() {
        HomeFragment homeFragment = new HomeFragment();
        fragmentManager.beginTransaction()
                .add(mainContainerId, homeFragment)
                .commit();
    }
}
