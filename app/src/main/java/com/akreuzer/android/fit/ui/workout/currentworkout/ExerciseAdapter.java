package com.akreuzer.android.fit.ui.workout.currentworkout;

import android.databinding.DataBindingUtil;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.WorkoutCurrentExerciseInfoListItemBinding;
import com.akreuzer.android.fit.databinding.WorkoutCurrentExerciseRecordListItemBinding;
import com.akreuzer.android.fit.ui.workout.currentworkout.exerciseviewholders.BaseExerciseViewHolder;
import com.akreuzer.android.fit.ui.workout.currentworkout.exerciseviewholders.ExerciseInfoViewHolder;
import com.akreuzer.android.fit.ui.workout.currentworkout.exerciseviewholders.ExerciseRecordViewHolder;
import com.akreuzer.android.fit.vo.ExerciseBaseModel;
import com.akreuzer.android.fit.vo.ExerciseInfo;
import com.akreuzer.android.fit.vo.ExerciseRecord;

import static com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseViewTypes.ViewType;
import java.util.List;

/**
 * Created by andrewkreuzer on 2018-01-17.
 */
public class ExerciseAdapter extends
        RecyclerView.Adapter<BaseExerciseViewHolder> {

    private final String TAG = ExerciseAdapter.class.getSimpleName();

    private List<? extends ExerciseBaseModel> exerciseList;

    private InfoCallback infoCallback;
    private RecordCallback recordCallback;

    public ExerciseAdapter(InfoCallback infoCallback, RecordCallback recordCallback) {
        this.infoCallback = infoCallback;
        this.recordCallback = recordCallback;
    }

    @Override
    public BaseExerciseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ViewType.EXERCISE_INFO_TYPE:
                WorkoutCurrentExerciseInfoListItemBinding infoBinding = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()), R.layout.workout_current_exercise_info_list_item,
                                parent, false);
                return new ExerciseInfoViewHolder(infoBinding, infoCallback);
            case ViewType.EXERCISE_RECORD_TYPE:
                WorkoutCurrentExerciseRecordListItemBinding recordBinding = DataBindingUtil
                        .inflate(LayoutInflater.from(parent.getContext()), R.layout.workout_current_exercise_record_list_item,
                                parent, false);
                return new ExerciseRecordViewHolder(recordBinding, recordCallback);

            default:
                //TODO
                return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(final BaseExerciseViewHolder holder, int position) {
        holder.bind(exerciseList.get(position));
    }

    @Override
    public int getItemCount() { return exerciseList == null ? 0 : exerciseList.size(); }

    @Override
    public int getItemViewType(int position) {
        return exerciseList.get(position).getViewType();
    }

    @SuppressWarnings("unchecked")
    public void setExerciseList(final List<? extends ExerciseBaseModel> newExerciseRecordList) {
        if (exerciseList == null) {
            exerciseList = newExerciseRecordList;
            notifyItemRangeInserted(0, newExerciseRecordList.size());
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ExerciseDiff(exerciseList,
                    newExerciseRecordList));
            exerciseList = newExerciseRecordList;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public interface InfoCallback {
        void onItemClicked(View v, ExerciseInfo exerciseInfo);
    }

    public interface RecordCallback {
        void onItemClicked(View v, ExerciseRecord exerciseRecord);
    }
}