package com.akreuzer.android.fit.ui.workout.currentworkout.exerciseviewholders;

import android.view.View;

import com.akreuzer.android.fit.databinding.WorkoutCurrentExerciseRecordListItemBinding;
import com.akreuzer.android.fit.ui.workout.currentworkout.ExerciseAdapter;
import com.akreuzer.android.fit.vo.ExerciseRecord;

/**
 * akreuzer on 16/03/18.
 */

public class ExerciseRecordViewHolder extends BaseExerciseViewHolder<ExerciseRecord> implements
        View.OnClickListener {
    final WorkoutCurrentExerciseRecordListItemBinding binding;
    private ExerciseRecord exerciseRecord;
    private ExerciseAdapter.RecordCallback callback;

    public ExerciseRecordViewHolder(WorkoutCurrentExerciseRecordListItemBinding binding,
                                    ExerciseAdapter.RecordCallback callback) {
        super(binding.getRoot());

        this.binding = binding;
        this.callback = callback;

        binding.getRoot().setOnClickListener(this);
    }

    @Override
    public void bind(ExerciseRecord exerciseRecord) {
        binding.setExercise(exerciseRecord);
        this.exerciseRecord = exerciseRecord;
    }

    @Override
    public void onClick(View v) {
        callback.onItemClicked(v, exerciseRecord);
    }
}
