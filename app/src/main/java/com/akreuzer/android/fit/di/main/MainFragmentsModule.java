package com.akreuzer.android.fit.di.main;

import com.akreuzer.android.fit.ui.main.pagerfragments.HomeFragment;
import com.akreuzer.android.fit.ui.main.pagerfragments.StatisticsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * akreuzer on 26/02/18.
 */

@Module
public abstract class MainFragmentsModule {
    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract StatisticsFragment contributeStatisticsFragment();

//    @ContributesAndroidInjector
//    abstract HomeFragment contributeHomeFragment();
}
