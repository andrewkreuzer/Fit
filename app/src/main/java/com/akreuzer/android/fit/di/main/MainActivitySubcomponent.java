package com.akreuzer.android.fit.di.main;

import com.akreuzer.android.fit.ui.main.MainActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * akreuzer on 26/02/18.
 */

@MainActivityScope
@Subcomponent(modules = MainFragmentsModule.class)
public interface MainActivitySubcomponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    public abstract class Builder extends AndroidInjector.Builder<MainActivity> {}
}
