package com.akreuzer.android.fit.db.exercises;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.akreuzer.android.fit.vo.ExerciseInfo;


/**
 * akreuzer on 21/02/18.
 */

@Database(entities = {ExerciseInfo.class}, version = 1)
public abstract class ExerciseInfoDatabase extends RoomDatabase {
    public abstract ExerciseInfoDao exerciseInfoDao();

    private static ExerciseInfoDatabaseCallback exerciseInfoDatabaseCallback = new ExerciseInfoDatabaseCallback();

    public static ExerciseInfoDatabase INSTANCE;

    public static ExerciseInfoDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ExerciseInfoDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ExerciseInfoDatabase.class, "exercise_database")
                            .addCallback(exerciseInfoDatabaseCallback)
                            .build();

                }
            }
        }
        return INSTANCE;
    }
}
