package com.akreuzer.android.fit.vo;

/**
 * akreuzer on 15/03/18.
 */

public interface ExerciseBaseModel {
    String getInfoId();
    String getName();
    int getViewType();
}
