package com.akreuzer.android.fit.db;

import android.arch.persistence.room.TypeConverter;

import com.akreuzer.android.fit.vo.ExerciseRecord;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * akreuzer on 22/02/18.
 */

public class StringListJsonConverter {
    @TypeConverter
    public static List<String> jsonToList(String json) {
        if (json == null) return Collections.emptyList();

        List<String> StringList = null;
        try {
            StringList = moshiAdapter().fromJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return StringList;
    }

    @TypeConverter
    public static String exerciseListToJson(List<String> StringList) {
        String json = moshiAdapter().toJson(StringList);

        return json;
    }

    private static JsonAdapter<List<String>> moshiAdapter() {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, String.class);

        return moshi.adapter(type);
    }
}
