package com.akreuzer.android.fit.ui.main;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.akreuzer.android.fit.BuildConfig;
import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.MainActivityBinding;
import com.akreuzer.android.fit.repository.ExerciseInfoRepository;
import com.akreuzer.android.fit.ui.main.pagerfragments.HomeFragment;
import com.akreuzer.android.fit.ui.main.pagerfragments.StatisticsFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MainViewModel mainViewModel;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    ExerciseInfoRepository exerciseInfoRepository;

    MainActivityBinding binding;

    @Inject
    MainActivityNavigation navigation;


    ViewPager viewPager;
    MainPagerAdapter pagerAdapter;
    BottomNavigationView bottomNavigationView;

    @Inject
    HomeFragment homeFragment;

    @Inject
    StatisticsFragment statisticsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);

        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

        Toolbar toolbar = binding.mainToolBar;
        setSupportActionBar(toolbar);

        viewPager = binding.mainViewPager;
        viewPagerSetup(viewPager);
        viewPager.addOnPageChangeListener(pageChangeListener);


        bottomNavigationView = binding.navigation;
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (BuildConfig.DEBUG) Timber.i("Main activity PageSelected %d", position);
            bottomNavigationView.getMenu().getItem(position).setChecked(true);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                viewPager.setCurrentItem(0);
                return true;
            case R.id.navigation_statistics:
                viewPager.setCurrentItem(1);
                return true;
            case R.id.navigation_notifications:
                return true;
        }
        return false;
    };


    private void viewPagerSetup(ViewPager viewPager) {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new HomeFragment());
        fragmentList.add(new StatisticsFragment());
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}

