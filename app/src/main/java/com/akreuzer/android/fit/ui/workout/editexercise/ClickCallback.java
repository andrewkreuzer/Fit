package com.akreuzer.android.fit.ui.workout.editexercise;

import android.view.View;

/**
 * akreuzer on 05/02/18.
 */

//TODO: might not need this
public interface ClickCallback {
    void onClick(View v);
}
