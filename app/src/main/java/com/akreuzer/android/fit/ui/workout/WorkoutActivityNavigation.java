package com.akreuzer.android.fit.ui.workout;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.ui.main.MainActivity;
import com.akreuzer.android.fit.ui.main.pagerfragments.HomeFragment;
import com.akreuzer.android.fit.ui.workout.currentworkout.CurrentWorkoutFragment;
import com.akreuzer.android.fit.ui.workout.editexercise.EditExerciseFragment;

import javax.inject.Inject;

/**
 * akreuzer on 08/02/18.
 */

//TODO: this has to be dealt with, its not a good way to do this
public class WorkoutActivityNavigation {
    private final Activity activity;
    private final int workoutContainerId;
    private final FragmentManager fragmentManager;

    @Inject
    public WorkoutActivityNavigation(WorkoutActivity workoutActivity) {
        this.activity = workoutActivity;
        this.workoutContainerId = R.id.workout_fragment_container;
        this.fragmentManager = workoutActivity.getSupportFragmentManager();
    }

    public void toEditExercise(@Nullable Bundle bundle) {
        EditExerciseFragment editExerciseFragment = new EditExerciseFragment();
        editExerciseFragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(workoutContainerId, editExerciseFragment)
                .addToBackStack("newExercise")
                .commit();
    }

    public void toCurrentWorkout() {
        CurrentWorkoutFragment currentWorkoutFragment = new CurrentWorkoutFragment();
        currentWorkoutFragment.setArguments(activity.getIntent().getExtras());
       fragmentManager.beginTransaction()
                .add(workoutContainerId, currentWorkoutFragment)
                .commit();
    }
}
