package com.akreuzer.android.fit.di.db;

import android.app.Application;

import com.akreuzer.android.fit.db.exercises.ExerciseInfoDao;
import com.akreuzer.android.fit.db.exercises.ExerciseInfoDatabase;
import com.akreuzer.android.fit.db.workouts.WorkoutDao;
import com.akreuzer.android.fit.db.workouts.WorkoutDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * akreuzer on 24/02/18.
 */
@Module
public class DatabaseModule {
    @Provides
    @Singleton
    WorkoutDatabase provideWorkoutDb(Application application) {
        return WorkoutDatabase.getDatabase(application);
    }

    @Provides
    @Singleton
    WorkoutDao provideWorkoutDao(WorkoutDatabase workoutDatabase) {
        return workoutDatabase.workoutDao();
    }

    @Provides
    @Singleton
    ExerciseInfoDatabase provideExerciseInfoDb(Application application) {
        return ExerciseInfoDatabase.getDatabase(application);
    }

    @Provides
    @Singleton
    ExerciseInfoDao providesExerciseInfoDao(ExerciseInfoDatabase exerciseInfoDatabase) {
        return exerciseInfoDatabase.exerciseInfoDao();
    }
}
