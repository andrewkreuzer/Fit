package com.akreuzer.android.fit.ui.main.pagerfragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akreuzer.android.fit.R;
import com.akreuzer.android.fit.databinding.MainFragmentStatisticsBinding;


import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * akreuzer on 14/02/18.
 */

public class StatisticsFragment extends Fragment {

    MainFragmentStatisticsBinding binding;

    @Inject
    public StatisticsFragment() {}

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment_statistics, container, false);
        View view = binding.getRoot();

        return view;
    }
}
