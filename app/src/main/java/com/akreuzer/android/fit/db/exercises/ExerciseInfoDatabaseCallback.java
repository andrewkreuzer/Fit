package com.akreuzer.android.fit.db.exercises;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.RoomDatabase;
import android.support.annotation.NonNull;

import timber.log.Timber;

/**
 * akreuzer on 21/02/18.
 */

public class ExerciseInfoDatabaseCallback extends RoomDatabase.Callback {
    public void onCreate(@NonNull SupportSQLiteDatabase db) {
        Timber.i("ExerciseInfoDatabase Created");
    }

    public void onOpen(@NonNull SupportSQLiteDatabase db) {
        //check for new additions
    }
}
