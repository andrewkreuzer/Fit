package com.akreuzer.android.fit.ui.main;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.akreuzer.android.fit.repository.ExerciseInfoRepository;
import com.akreuzer.android.fit.vo.Workout;
import com.akreuzer.android.fit.repository.WorkoutRepository;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by andrewkreuzer on 2018-01-17.
 */

public class MainViewModel extends AndroidViewModel {
    private WorkoutRepository workoutRepository;
    private ExerciseInfoRepository exerciseInfoRepository;

    private LiveData<List<Workout>> allWorkouts;

    @Inject
    public MainViewModel(Application application, WorkoutRepository workoutRepository) {
        super(application);
        this.workoutRepository = workoutRepository;
        allWorkouts = workoutRepository.getAllWorkouts();

//        exerciseInfoRepository = new ExerciseInfoRepository(application, exerciseInfoService);
    }



    LiveData<List<Workout>> getAllWorkouts() { return allWorkouts; }

    public void insert(Workout workout) { workoutRepository.insert(workout); }
}
