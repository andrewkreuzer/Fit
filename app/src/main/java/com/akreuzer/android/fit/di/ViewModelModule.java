package com.akreuzer.android.fit.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.akreuzer.android.fit.ui.main.MainViewModel;
import com.akreuzer.android.fit.ui.workout.WorkoutViewModel;
import com.akreuzer.android.fit.ui.workout.currentworkout.CurrentWorkoutViewModel;
import com.akreuzer.android.fit.ui.workout.editexercise.EditExerciseViewModel;
import com.akreuzer.android.fit.viewmodel.FitViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * akreuzer on 05/03/18.
 */

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(WorkoutViewModel.class)
    abstract ViewModel bindWorkoutViewModel(WorkoutViewModel workoutViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(EditExerciseViewModel.class)
    abstract ViewModel bindEditExerciseViewModel(EditExerciseViewModel editExerciseViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CurrentWorkoutViewModel.class)
    abstract ViewModel bindCurrentWorkoutViewModel(CurrentWorkoutViewModel currentWorkoutViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(FitViewModelFactory factory);
}
