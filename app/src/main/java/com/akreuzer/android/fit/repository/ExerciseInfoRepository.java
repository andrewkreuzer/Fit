package com.akreuzer.android.fit.repository;

import android.arch.lifecycle.LiveData;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.akreuzer.android.fit.api.ExerciseInfoService;
import com.akreuzer.android.fit.db.exercises.ExerciseInfoDao;
import com.akreuzer.android.fit.db.exercises.ExerciseInfoDatabase;
import com.akreuzer.android.fit.ui.workout.currentworkout.CurrentWorkoutViewModel;
import com.akreuzer.android.fit.vo.ExerciseInfo;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * akreuzer on 21/02/18.
 */

@Singleton
public class ExerciseInfoRepository {

    private final ExerciseInfoDatabase db;
    private final ExerciseInfoDao exerciseInfoDao;
    private final ExerciseInfoService exerciseInfoService;
    //TODO: don't inject sharedPreferences into the repository
    private final SharedPreferences sharedPreferences;

    @Inject
    public ExerciseInfoRepository(ExerciseInfoDatabase db,
                                  ExerciseInfoDao exerciseInfoDao,
                                  ExerciseInfoService exerciseInfoService,
                                  SharedPreferences sharedPreferences) {
        this.db = db;
        this.exerciseInfoDao = exerciseInfoDao;
        this.exerciseInfoService = exerciseInfoService;
        this.sharedPreferences = sharedPreferences;
    }

    public LiveData<List<ExerciseInfo>> exerciseSearch(String query) {
        return exerciseInfoDao.search('%' + query + '%');
    }

    public LiveData<ExerciseInfo> getExercise(String name) {
        return exerciseInfoDao.getExercise(name);
    }

    public void insert(ExerciseInfo... exerciseInfoEntities) {
        Executors.newSingleThreadExecutor().execute(() -> exerciseInfoDao.insert(exerciseInfoEntities));
    }

    public void fillExerciseInfoDatabase() {
        Call<ExerciseInfo[]> exerciseCall = exerciseInfoService.getFullExerciseList();
        exerciseCall.enqueue(new Callback<ExerciseInfo[]>() {
            @Override
            public void onResponse(@NonNull Call<ExerciseInfo[]> call, @NonNull Response<ExerciseInfo[]> response) {
                insert(response.body());
                sharedPreferences.edit().putBoolean("ExerciseInfoDatabaseFilled", true).apply();
            }

            @Override
            public void onFailure(@NonNull Call<ExerciseInfo[]> call, @NonNull Throwable t) {
                Timber.e(t, "Retrofit failed to get exercises");
                sharedPreferences.edit().putBoolean("ExerciseInfoDatabaseFilled", false).apply();
            }
        });
    }
}
