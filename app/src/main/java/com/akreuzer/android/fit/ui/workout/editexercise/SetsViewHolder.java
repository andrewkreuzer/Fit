package com.akreuzer.android.fit.ui.workout.editexercise;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.akreuzer.android.fit.databinding.WorkoutEditExerciseSetListItemBinding;
import com.akreuzer.android.fit.vo.Set;

/**
 * akreuzer on 07/04/18.
 */
public class SetsViewHolder extends RecyclerView.ViewHolder {
    final WorkoutEditExerciseSetListItemBinding binding;
    final SetsAdapter.TextChangeCallback callback;
    private EditText repsEditText;
    private EditText weightEditText;
    private Set set;

    SetsViewHolder(WorkoutEditExerciseSetListItemBinding binding,
                   SetsAdapter.TextChangeCallback callback) {
        super(binding.getRoot());
        this.binding = binding;
        this.callback = callback;
        setupTextChangeListeners();
    }

    public void bind(Set set) {
        binding.setSetInfo(set);
        this.set = set;
    }



    private void setupTextChangeListeners() {
        repsEditText = binding.repsEditText;
        weightEditText = binding.weightEditText;
//        notesEditText = binding.editExerciseNotes;
        repsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                String reps = s.toString();
                if (reps.isEmpty()) return;
                set.setReps(Integer.parseInt(reps));
                callback.onRepsChange(Integer.parseInt(reps));
            }
        });

        weightEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                String weight = s.toString();
                if (weight.isEmpty()) return;
                set.setWeight(Integer.parseInt(weight));
                callback.onWeightChange(Integer.parseInt(weight));
            }
        });

//        notesEditText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {}
//            @Override
//            public void afterTextChanged(Editable s) {
//                editExerciseViewModel.setExerciseNotes(s.toString());
//            }
//        });
    }
}
