package com.akreuzer.android.fit.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.akreuzer.android.fit.db.workouts.DateTimeConverter;
import com.akreuzer.android.fit.db.workouts.ExerciseRecordConverter;

import java.util.Date;
import java.util.List;

/**
 * Created by andrewkreuzer on 2018-01-12.
 */

@Entity
public class Workout {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @TypeConverters(DateTimeConverter.class)
    private Date dateTime;

    @TypeConverters(ExerciseRecordConverter.class)
    private List<ExerciseRecord> exercisesList;

    public Workout(Date dateTime) {
        this.dateTime = dateTime;
    }

    public void setId(int id) { this.id = id; }
    public int getId() { return id; }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public List<ExerciseRecord> getExercisesList() {
        return exercisesList;
    }

    public void setExercisesList(List<ExerciseRecord> exercisesList) {
        this.exercisesList = exercisesList;
    }


}
